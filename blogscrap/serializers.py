from rest_framework import serializers
from blogscrap.models import Stats

class StatsSerializer(serializers.Serializer):
    word = serializers.CharField()
    count = serializers.IntegerField()

    def create(self, validated_data):

        return Stats.objects.create(**validated_data)
