from django.apps import AppConfig
from blogscrap import soup
import sys

class BlogscrapConfig(AppConfig):
    name = 'blogscrap'

    def ready(self):
        if not 'runserver' in sys.argv:
            return True

        from blogscrap.models import Posturls, Stats
        posts_urls = []
        saved_urls = []
        new_posts = []
        posts_content = []

        posts_urls = soup.get_posts_urls()
        saved_urls = list(Posturls.objects.all())
        for p_url in posts_urls:
            for s_url in saved_urls:
                if p_url == s_url.post:
                    break
            else:
                new_posts.append(p_url)

        for url in new_posts:
            p = Posturls(post = url)
            p.save()

        print(len(new_posts))
        posts_content = soup.get_posts_content(new_posts)

        for word in posts_content:
            w = Stats(author = word[0], word = word[1])
            w.save()

        Stats.objects.filter(word__iexact='').delete()
