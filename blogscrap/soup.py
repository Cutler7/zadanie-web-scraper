from urllib.request import urlopen
from bs4 import BeautifulSoup as soup

base_url = "http://teonite.com"
marks = ',."-”“:;?!"”()[]„\/|<>#+-*•{}–='

ltrPL = "ŻÓŁĆĘŚĄŹŃżółćęśąźń"
ltrnoPL = "ZOLCESAZNzolcesazn"
trantab = str.maketrans(ltrPL, ltrnoPL)

def format_author(ex):
	ex = ex.replace(" ", "")
	ex = ex.lower()
	ex = ex.translate(trantab)
	return ex

def get_doc(url):
	data = urlopen(url)
	html_doc = data.read()
	data.close()
	html_doc = soup(html_doc, "lxml")
	return html_doc

def get_posts_urls():
	posts_urls = []
	next_page = ""
	main_page = get_doc(base_url + "/blog")
	while True:
		posts_links = main_page.find_all("a", {"class":"read-more"})
		for link in posts_links:
			posts_urls.append(link.get("href"))
		next_page = main_page.find("a", {"class":"older-posts"})
		if(next_page == None):
			break
		next_page = next_page.get("href")
		main_page = get_doc(base_url + next_page)
	return(posts_urls)

def get_posts_content(urls):
	posts_content = []
	for post in urls:
		print(base_url + post)
		current_post = get_doc(base_url + post)
		post_author = current_post.find("span", {"class":"author-content"}).find("h4").get_text()
		post_author = format_author(post_author)
		post_content = current_post.find("section", {"class":"post-content"}).get_text().split()
		for word in post_content:
			word = word.strip(marks).lower()
			posts_content.append((post_author, word))
	return(posts_content)
    