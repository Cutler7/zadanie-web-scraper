from django.conf.urls import url
from blogscrap import views

urlpatterns = [
    url(r'^stats/?$', views.StatsList.as_view()),
    url(r'^stats/(?P<author>\w+)/?$', views.StatsAuthorList.as_view())
]
