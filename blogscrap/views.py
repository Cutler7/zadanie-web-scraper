from blogscrap.models import Stats
from django.db.models import Count
from blogscrap.serializers import StatsSerializer
from rest_framework.views import APIView
from rest_framework.response import Response

class StatsList(APIView):

    def get(self, request, format=None):
        data = Stats.objects.values('word').annotate(count=Count('word')).order_by('-count')[:10]
        serializer = StatsSerializer(data, many=True)
        stats = {}
        for i in range(len(serializer.data)):
            word = serializer.data[i]['word']
            stats[word] = serializer.data[i]['count']
        return Response(stats)

class StatsAuthorList(APIView):

    def get(self, request, author, format=None):
        data = Stats.objects.filter(author__iexact=author).values('word').annotate(count=Count('word')).order_by('-count')[:10]
        serializer = StatsSerializer(data, many=True)
        stats = {}
        for i in range(len(serializer.data)):
            word = serializer.data[i]['word']
            stats[word] = serializer.data[i]['count']
        return Response(stats)
